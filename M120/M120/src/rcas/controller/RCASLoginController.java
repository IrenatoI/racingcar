package rcas.controller;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Base64;
import java.util.ResourceBundle;

public class RCASLoginController {

    public Button btnSubmit;
    public TextField Username;
    public PasswordField Password;
    @FXML
    private GridPane mainPane;
    @FXML
    private LineChart<Number, Number> mainChart;
    @FXML
    private ResourceBundle resources;

    @FXML
    private void initialize() {
        btnSubmit.setOnAction(e -> {
            String user = Username.getText();
            String password = Password.getText();
            login(user, password);
        });
    }

    private static void login(String user, String passwd) {
        try
        {
            String query = "SELECT * FROM Person WHERE Username = '<user>'";
            query = query.replaceAll("<user>", user);
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn;


            conn = DriverManager.getConnection("jdbc:mysql://localhost/m120?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            System.out.println("Database is connected !");
            PreparedStatement ps;
            ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
            {
                String salt = rs.getString("Salt");
                String Password = generateSecurePassword(passwd, salt);
                if (Password.equals(rs.getString("Password"))){
                    System.out.println("login ok");
                }
            }
            conn.close();
        }
        catch(Exception e)
        {
            System.out.print("Do not connect to DB - Error:"+e);
        }
    }

    //region Password
    private static final int ITERATIONS = 10000;
    private static final int KEY_LENGTH = 256;

    private static byte[] hash(char[] password, byte[] salt) {
        PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
        Arrays.fill(password, Character.MIN_VALUE);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new AssertionError("Error while hashing a Password: " + e.getMessage(), e);
        } finally {
            spec.clearPassword();
        }
    }

    private static String generateSecurePassword(String password, String salt) {
        String returnValue;
        byte[] securePassword = hash(password.toCharArray(), salt.getBytes());

        returnValue = Base64.getEncoder().encodeToString(securePassword);

        return returnValue;
    }
    //endregion
}
