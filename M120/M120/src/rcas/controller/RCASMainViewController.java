package rcas.controller;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;
import rcas.model.RaceCar;
import rcas.model.RaceCarSelectionItem;
import rcas.util.CorneringAnalyserUtil;
import rcas.util.DataUtil;

import java.io.IOException;
import java.util.ResourceBundle;

public class RCASMainViewController {

	@FXML
	public TableView carsTableView;

	private ObservableList<RaceCarSelectionItem> tableViewData;
	private ObservableList<String> cbValues = FXCollections.observableArrayList("RED", "BROWN", "GREEN", "BLUE");
    @FXML
	private GridPane mainPane;
	@FXML
	private LineChart<Number, Number> mainChart;
	@FXML
	private ResourceBundle resources;
	@FXML
	private Button btnAddNew;
	@FXML
	private Button btnChange;
	@FXML
	private Button btnDelete;

	@FXML
	public void initialize() {
		carsTableView.setEditable(true); // Makes the table view Editable

		TableColumn colSelection = new TableColumn("Selection");
		TableColumn colCarName = new TableColumn("Car Name");

		tableViewData = FXCollections.observableArrayList();
		tableViewData.addAll(DataUtil.GetAllRaceCarSelectionItems());
		colSelection.setCellValueFactory((Callback<TableColumn.CellDataFeatures<RaceCarSelectionItem, Boolean>, ObservableValue<Boolean>>) param -> {
			RaceCarSelectionItem raceCarSelectionItem = param.getValue();
			SimpleBooleanProperty booleanProperty = new SimpleBooleanProperty(raceCarSelectionItem.getIsSelected());
			booleanProperty.addListener((observable, oldValue, newValue) -> {
				raceCarSelectionItem.setIsSelected(newValue);
				setDiagramForAllSelectedCars();
			});
			return booleanProperty;
		});
		colSelection.setCellFactory((Callback<TableColumn<RaceCarSelectionItem, Boolean>, TableCell<RaceCarSelectionItem, Boolean>>) p -> {
			CheckBoxTableCell<RaceCarSelectionItem, Boolean> cell = new CheckBoxTableCell<>();
			cell.setAlignment(Pos.CENTER);
			return cell;
		});

		colCarName.setCellValueFactory(new PropertyValueFactory<>("raceCarName"));

        TableColumn colColour = new TableColumn<>("Color");
		colColour.setCellValueFactory((Callback<TableColumn.CellDataFeatures<RaceCarSelectionItem, String>, ObservableValue<String>>) param -> {
			RaceCarSelectionItem raceCarSelectionItem = param.getValue();
			SimpleStringProperty stringProperty = new SimpleStringProperty(raceCarSelectionItem.getGridColor());
			stringProperty.addListener((observable, oldValue, newValue) -> {
				raceCarSelectionItem.setGridColor(newValue);
				setDiagramForAllSelectedCars();
			});
			return stringProperty;
		});

        colColour.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(), cbValues));

        carsTableView.getColumns().addAll(colSelection, colCarName, colColour);
		carsTableView.setItems(tableViewData);

		setDiagramForAllSelectedCars();

		btnAddNew.setOnAction(e -> openCarEdit(new RaceCar("New Car")));
		btnChange.setOnAction(e -> {
			RaceCarSelectionItem rcsi = (RaceCarSelectionItem)carsTableView.getSelectionModel().getSelectedItem();
			RaceCar rc = rcsi.getRaceCar();
			openCarEdit(rc);
		});
		btnDelete.setOnAction(event -> tableViewData.remove(carsTableView.getSelectionModel().getSelectedItem()));
	}

	private void setDiagramForAllSelectedCars(){
		mainChart.getData().removeAll();
		mainChart.getData().clear();
		for(RaceCarSelectionItem rcsi : tableViewData){
			if(rcsi.getIsSelected()){
				CorneringAnalyserUtil corneringUtil = new CorneringAnalyserUtil();
				ObservableList<Series<Number, Number>> dataList = corneringUtil.generateMMMChartData(rcsi.getRaceCar());
				mainChart.getData().addAll(dataList);
				this.setSeriesStyle(dataList, ".chart-series-line", "-fx-stroke: "+rcsi.getGridColor() +"; -fx-stroke-width: 1px;");
			}
		}
	}
	private void setSeriesStyle(ObservableList<Series<Number, Number>> dataList_1, String styleSelector,
			String lineStyle) {
		for (Series<Number, Number> curve : dataList_1) {
			curve.getNode().lookup(styleSelector).setStyle(lineStyle);
		}
	}

	private void openCarEdit(RaceCar car) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/rcas/view/RCASCarEdit.fxml"));
		GridPane root;
		try {
			root = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Car Details");
			stage.setScene(new Scene(root, 725, 530));

			RCASCarEdit controller = fxmlLoader.getController();
			controller.SetCarForEdit(car);

			Scene scene;
			if (root.getScene() == null) {
				scene = new Scene(root);
				stage.setScene(scene);
			} else {
				stage.setScene(root.getScene());
			}
			stage.show();
		}
		catch (IOException error) {
			error.printStackTrace();
		}
	}

	private void printRaceCarCorneringValues(RaceCar raceCar, CorneringAnalyserUtil util) {
		System.out.println(String.format(
				"%s: Grip = %.2f G\tBalance = %.2f Nm\tControl(entry) = %.2f Nm/deg\t"
						+ "Control(middle) = %.2f Nm/deg\tStability(entry) = %.2f Nm/deg\t"
						+ "Stability(middle) = %.2f Nm/deg",
				raceCar.getName(), util.getMMMGripValue(raceCar) / 9.81, util.getMMMBalanceValue(raceCar),
				util.getMMMControlValue(raceCar, 0.0, 0.0, 10.0), util.getMMMControlValue(raceCar, -5.0, 20.0, 30.0),
				util.getMMMStabilityValue(raceCar, 0.0, 0.0, 1.0),
				util.getMMMStabilityValue(raceCar, 20.0, -5.0, -4.0)));
	}
}
