package rcas.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import rcas.model.RaceCar;

public class RCASCarEdit {


    public TextField Name;
    public TextField Wheelbase;
    public TextField CoG;
    public TextField Suspensionheight;
    public TextField FTWidth;
    public TextField RTWidth;
    public TextField FATire;
    public TextField RATire;
    public TextField FL;
    public TextField FR;
    public TextField RL;
    public TextField RR;
    public Button Submit;
    public ImageView boximage1;
    public ImageView boximage;
    private RaceCar raceCar;

    public void SetCarForEdit(RaceCar car) {
        raceCar = car;

        if (!car.getName().equals("New Car")){
            Name.setText(car.getName());
            Wheelbase.setText(String.valueOf(car.getWheelbase()));
            CoG.setText(String.valueOf(car.getCogHeight()));
            Suspensionheight.setText(String.valueOf(car.getFrontRollDist()));
            FTWidth.setText(String.valueOf(car.getFrontTrack()));
            RTWidth.setText(String.valueOf(car.getRearTrack()));
            //TODO Tire Model needs rework for readable Data
            FATire.setText(String.valueOf(car.getFrontAxleTireModel()));
            RATire.setText(String.valueOf(car.getRearAxleTireModel()));
            FL.setText(String.valueOf(car.getCornerWeightFL()));
            FR.setText(String.valueOf(car.getCornerWeightFR()));
            RL.setText(String.valueOf(car.getCornerWeightRL()));
            RR.setText(String.valueOf(car.getCornerWeightRR()));
            EditCar();
        }
        else {
            Name.setText(car.getName());
            CreateNewCar();
        }
    }

    private void CreateNewCar() {
        Submit.setOnAction(e -> {
            save();
        });
    }

    private void EditCar(){
        Submit.setOnAction(e -> {
            save();
        });
    }

    private ObservableList<String> getTireNameList() {
        ObservableList<String> tireList = FXCollections.observableArrayList();
        tireList.add("BFGoodrich");
        tireList.add("Bridgestone");
        tireList.add("Continental");
        tireList.add("Cooper");
        tireList.add("Dunlop");
        tireList.add("Falken");
        tireList.add("Firestone");
        tireList.add("General");
        tireList.add("Goodyear");
        tireList.add("GT Radial");
        tireList.add("Hankook");
        tireList.add("Kumho");
        tireList.add("Michelin");
        tireList.add("Nexen");
        tireList.add("Nitto");
        tireList.add("Nokian");
        tireList.add("Pirelli");
        tireList.add("Sumitomo");
        tireList.add("Toyo");
        tireList.add("Uniroyal");
        tireList.add("Yokohama");

        return tireList;
    }

    private void save() {

    }
}
